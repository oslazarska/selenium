package ru.tsystems.mshop.qa.selenium;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BaseSeleniumTest {

    @Test
    public void testSumAndMultiplication() {
        int x = 2;
        int result = x + x * 6;
        assertThat("Check sum and multiplication", result, is(14));

    }
}
